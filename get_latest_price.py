#!/usr/bin/env python3
# refresh_token is good for 90 days - after that have to get a new one per procedure in
# ~/thinkorswim/OAuth2_method  and https://www.youtube.com/watch?v=qJ94sSyPGBw
import requests
import sys
from datetime import datetime, timedelta
import configparser

config = configparser.ConfigParser()
config.read('config.ini')

if ( len(sys.argv) != 2 ):
   print("Usage: ./get_latest_price.py 'TICKER'")
   sys.exit(-1)

# get an access_token using current refresh_token
url = r'https://api.tdameritrade.com/v1/oauth2/token'

# define the headers
headers = {"Content-Type":"application/x-www-form-urlencoded"}

# define the payload - client_id does NOT need the '@AMER.OAUTHAP' as when getting a refresh_token
payload = {'grant_type': 'refresh_token',  
           'refresh_token': config['creds']['refresh_token'], 
           'client_id':config['creds']['client_id'], 
           'redirect_uri':'https://127.0.0.1'}


# post the data to get the token
authReply = requests.post(url, headers = headers, data=payload)

# convert it to a dictionary
decoded_content = authReply.json()

# grab the access_token
access_token = decoded_content['access_token']
headers = {'Authorization': "Bearer {}".format(access_token)}

endpoint = r"https://api.tdameritrade.com/v1/marketdata/{}/pricehistory".format(sys.argv[1].upper())

earlier8h = datetime.now() - timedelta(hours=8)
payload = {'periodType':'day',
           'frequencyType':'minute',
           'frequency':'1',
           'endDate':str(datetime.now().timestamp()*1000).split('.')[0],
           'startDate':str(earlier8h.timestamp()*1000).split('.')[0]
          }

content = requests.get(url = endpoint, headers = headers, params = payload)
data = content.json()

print(data['symbol'])
print('close','\t\t','datetime')
candles = data['candles']
for ndx in range(len(candles)-10,len(candles)):
   print(candles[ndx]['close'],'\t\t',datetime.fromtimestamp(candles[ndx]['datetime']/1000.))


