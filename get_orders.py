#!/usr/bin/env python3
# refresh_token is good for 90 days - after that have to get a new one per procedure in
# ~/thinkorswim/OAuth2_method  and https://www.youtube.com/watch?v=qJ94sSyPGBw

import requests
from datetime import datetime, timedelta
import configparser
import sys

if ( len(sys.argv) != 2 ):
  print("Usage: ./get_orders.py <status: QUEUED, FILLED, CANCELED, etc>")
  # all valid status 'FILLED', 'EXPIRED', 'AWAITING_PARENT_ORDER', 'AWAITING_CONDITION', 'AWAITING_MANUAL_REVIEW', 'ACCEPTED', 'AWAITING_UR_OUT', 'PENDING_ACTIVATION', 'QUEUED', 'WORKING', 'REJECTED', 'PENDING_CANCEL', 'CANCELED', 'PENDING_REPLACE', 'REPLACED'
  sys.exit(-1)

config = configparser.ConfigParser()
config.read('config.ini')


# get an access_token using current refresh_token
url = r'https://api.tdameritrade.com/v1/oauth2/token'

# define the headers
headers = {"Content-Type":"application/x-www-form-urlencoded"}

# define the payload - client_id does NOT need the '@AMER.OAUTHAP' as when getting a refresh_token
payload = {'grant_type': 'refresh_token',  
           'refresh_token': config['creds']['refresh_token'], 
           'client_id':config['creds']['client_id'], 
           'redirect_uri':'https://127.0.0.1'}


# post the data to get the token
authReply = requests.post(url, headers = headers, data=payload)

# convert it to a dictionary
decoded_content = authReply.json()

# grab the access_token
access_token = decoded_content['access_token']
headers = {'Authorization': "Bearer {}".format(access_token)}

endpoint = r"https://api.tdameritrade.com/v1/orders"

payload = {'accountId':config['creds']['account_number'],'status':sys.argv[1]}
#           'maxResults':'5',
#           'fromEnteredTime':'2020-06-15',
#           'toEnteredTime':'2020-06-16'}

content = requests.get(url = endpoint, headers = headers, params = payload)
data = content.json()

print(data)


