#!/usr/bin/env python3

import requests
import urllib
import sys
import configparser

config = configparser.ConfigParser()
config.read('config.ini')

if ( len(sys.argv) != 2 ):
  print("Usage: ./get_movers 'exchange: $SPX.X, $DJI or $COMPX'")
  sys.exit(-1)

endpoint = r"https://api.tdameritrade.com/v1/marketdata/{}/movers".format(urllib.parse.quote(sys.argv[1]))

payload = {'apikey':config['creds']['client_id']}

content = requests.get(url = endpoint, params = payload)

data = content.json()

print(data)

