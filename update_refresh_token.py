#!/usr/bin/env python3
#  update the refresh_token in config.ini from logging into the browser

import configparser
from splinter import Browser
import requests
import time
import urllib
import sys
from datetime import datetime

config = configparser.ConfigParser()
config.read('config.ini')

executable_path = {'executable_path': r'/usr/bin/chromedriver'}

browser = Browser('chrome', **executable_path, headless=False)

method = 'GET'
url = 'https://auth.tdameritrade.com/auth?'
client_code = config['creds']['client_id'] + '@AMER.OAUTHAP'
payload = {'response_type':'code', 'redirect_uri':'https://127.0.0.1', 'client_id':client_code}

p = requests.Request(method, url, params=payload).prepare()
myurl = p.url

browser.visit(myurl)
time.sleep(1)

# comes up ok, trying this
username = browser.find_by_id("username").first.fill(config['creds']['username'])
password = browser.find_by_id("password").first.fill(config['creds']['password'])
submit   = browser.find_by_id("accept").first.click()

# still had to go thru the sms code verification
time.sleep(1)
submit   = browser.find_by_id("accept").first.click()
print("Enter sms code just texted")
smscode = sys.stdin.readline().strip()
smsret = browser.find_by_id("smscode").first.fill(smscode)
submit   = browser.find_by_id("accept").first.click()
submit   = browser.find_by_id("accept").first.click()

new_url = browser.url 
parse_url = urllib.parse.unquote(new_url).split('code=')[1]

# done with the browser
browser.quit()

# that parse_url is the code we use to pass thru new requests
# to https://api.tdameritrade.com/v1/oauth2/token
url = r'https://api.tdameritrade.com/v1/oauth2/token'

# define the headers
headers = {"Content-Type":"application/x-www-form-urlencoded"}

# define the payload - client_id does NOT need the '@AMER.OAUTHAP' as above
payload = {'grant_type': 'authorization_code', 
           'access_type': 'offline', 
           'code': parse_url, 
           'client_id':config['creds']['client_id'], 
           'redirect_uri':'https://127.0.0.1'}

# post the data to get the token
authReply = requests.post(url, headers = headers, data=payload)

# convert it to a dictionary
decoded_content = authReply.json()

# save it in config.ini
config['creds']['refresh_token'] = decoded_content['refresh_token']
# with a timestamp
config['creds']['token_timestamp'] = str(datetime.now().timestamp()).split('.')[0]
with open('config.ini', 'w') as configfile:
   config.write(configfile)


