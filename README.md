# TDAscripts

Authentication process created from watching 	https://www.youtube.com/watch?v=qJ94sSyPGBw   <br/><br/>

example python3 scripts to obtain an acccess token, good for 30 minutes and a refresh token, good for 90 days using multifactor authentication.  With a valid refresh token stored in a ConfigParser ini file various actions can be performed like fetch up to date market data, get account details like value, etc. <br/> <br/>
See the API documentation at https://developer.tdameritrade.com/apis for ALL options, these are just a small, but working slice of possibilities. <br/><br/>

# Scripts<br/>

update config.ini with your account and you will need to create an account and app at https://developer.tdameritrade.com/  . To match these scripts use for callback url:  http://127.0.0.1   that has to match what is in the script.  The account number is nine digit without anything like 'TDA' at the end. You could possible change update_refresh_token.py to just enter those credentials yourself without saving them in a config.ini file for increased security. Added a token_timestamp to keep track of when we need to update the refresh token after 90 days. <br/><br/>

update_refresh_token.py - where to start if you have no refresh token. Might need change if you do not use sms text 2 factor authentication, which you should. Opens a chrome browser to the TDAmeritrade login page and fills in username and password and clicks SUBMIT for you then clicks thru to send an sms text with a code. The script asks for your sms texted code and enters it on the page, gets the auth code, closes the browser and accesses another endpoint with the auth code to get an access token and refresh token. The refresh token and timestamp is saved in config.ini <br/><br/>

get_access_token.py - just uses the 90 day refresh token to get a 30 minute access token, maybe to use with the API guide examples where it asks for authorization, enter Bearer and big long access token   without any quotes etc. Prints out the expiration day of the refresh token also. <br/><br/>

get_acct_value.py - prints the liquidation value of your account, just playing with what is avaiable<br/><br/>

get_movers.py - run this with no arguments for suggestions, run it with '$SPX.X' (single quotes needed, not double, single, so the $ does not try to get a shell substitution) , '$COMPX' or '$DJI'   -- returms list of top movers up or down <br/><br/>

get_latest_price.py - run with an UPPERCASE TICKER like OKE etc to get some recent minute data without delay.  Uses authentication to get recent data. It is possible to get data with just the api key but it is delayed 15 minutes <br/><br/>

get_quotes.py - takes a list of tickers like 'MU,OKE' and returns quote info. Included authentication from refresh token so they are NOT delayed<br/><br/>

get_orders.py - takes status (QUEUED is 'WORKING' in ThinkOrSwim, oddly, FILLED, CANCELED, etc - see script for list) and retrieves orders of matching status. Used to get the orderId for cancelling orders, can have other parameters like after date, before date, etc. <br/><br/>

cancel_order.py - takes an orderId and DELETEs it<br/><br/>

place_order.py - takes a TICKER symbol and a limit price and places an order <br/><br/>


