#!/usr/bin/env python

import requests
import sys
import configparser

config = configparser.ConfigParser()
config.read('config.ini')

if ( len(sys.argv) != 2 ):
  print("Usage: ./get_quotes 'sym1,sym2,...'")
  sys.exit(-1)


# get an access_token using current refresh_token
url = r'https://api.tdameritrade.com/v1/oauth2/token'

# define the headers
headers = {"Content-Type":"application/x-www-form-urlencoded"}

# define the payload - client_id does NOT need the '@AMER.OAUTHAP' as when getting a refresh_token
payload = {'grant_type': 'refresh_token',
           'refresh_token': config['creds']['refresh_token'],
           'client_id':config['creds']['client_id'],
           'redirect_uri':'https://127.0.0.1'}


# post the data to get the token
authReply = requests.post(url, headers = headers, data=payload)

# convert it to a dictionary
decoded_content = authReply.json()

# grab the access_token
access_token = decoded_content['access_token']
headers = {'Authorization': "Bearer {}".format(access_token)}



endpoint = r"https://api.tdameritrade.com/v1/marketdata/quotes"

payload = {'apikey':config['creds']['client_id'],'symbol':sys.argv[1].upper()}

content = requests.get(url = endpoint, headers = headers, params = payload)

data = content.json()

print(data)

